window.onscroll = function (event){
    let navbar = document.querySelector(".presto-navbar")
    if (document.documentElement.scrollTop > 50){
        navbar.classList.add("padding-extension")
        navbar.classList.add("border-shadow")
    }else{
        navbar.classList.remove("padding-extension")
        navbar.classList.remove("border-shadow")
    }
}

// CON fetch inserisco l'inidirizzo del mio file json
fetch('./annunci.json')
// .then lo converto in array di oggetti
.then((response) => response.json())
.then((annunci) => {
// utiliziamo set per creare un array di categorie univoche, così che, quando incontriamo due categorie uguali, non venogno ripetute
    let categories = new Set()
    annunci.forEach((el) => categories.add(el.category))
// ciclo le mie categorie
categories.forEach(function(category){
// mi creo un div
    let categoryElement = document.createElement("div")
// do delle classi per poterle personalizzare 
    categoryElement.className= "presto-card presto-text text-center d-flex flex-column m-4 text-white"
// scrivo cosa mettere dentro il div appena creato
    categoryElement.innerHTML =
    `
    <p class="h2 mt-3">
        ${category}
    </p>
    `
// seleziono il contenitore dove inserirle
    let categoriesWrapper = document.querySelector("#categories-wrapper")
// e le aggancio
    categoriesWrapper.appendChild(categoryElement)
    })

    // metto in ordine decrescente gli annunci 
    annunci.sort((a, b) => b.id - a.id)
    
    annunci.slice(0, 4).forEach(function (annuncio){
    let announcementElement = document.createElement("div")
    announcementElement.classList.add("glide_slide")
    announcementElement.classList.add("glide-card-slide")
    
    announcementElement.innerHTML =
    // mi creo la mia card utilizzando come riferimento il mio file json

    `
    <h4 class="fw-bolder"> ${annuncio.category} </h4>
   <img class="rounded rounded-3 p4 border border-dark img-last-ann" src="https://picsum.photos/200"
   <div class="d-flex justify-content-between"
        <span> ${annuncio.name} </span>
        <span>${annuncio.price}</span>
    </div>

    `

    let glideSlideWrapper = document.querySelector(".glide .glide__track .glide__slides")
    glideSlideWrapper.appendChild(announcementElement)
    })

// carosello di glide
    new Glide('.glide', {
        type: 'carousel',
        startAt: 0,
        perView: 1
    }).mount()
})