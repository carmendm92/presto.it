// funzione per la nav bar

// allo scroll del mause
window.onscroll = function (event){
    // seleziono la nav bar
    let navbar = document.querySelector(".presto-navbar")
    // se lo scroll è maggiore di 50
    if (document.documentElement.scrollTop > 50){
        // aggiungi alla nav bar due classi
        navbar.classList.add("padding-extension")
        navbar.classList.add("border-shadow")
        // se no rimuovi le due classi
    }else{
        navbar.classList.remove("padding-extension")
        navbar.classList.remove("border-shadow")
    }
}

// CON fetch inserisco l'inidirizzo del mio file json
fetch('./annunci.json')
// .then lo converto in array di oggetti
.then((response) => response.json())
.then((annunci) => {

// utilizziamo new Set per creare un array di categorie univoche, così che, quando incontriamo due categorie uguali, non venogno ripetute
    let categories = new Set()
    annunci.forEach((el) => categories.add(el.category))
// ciclo le mie categorie
    categories.forEach(function(category){
// mi creo un div
    let categoryElement = document.createElement("div")
    categoryElement.classList.add("presto-category-find")
// scrivo cosa mettere dentro il div appena creato
    categoryElement.innerHTML =
    
     // il contenuto del mio div, in questo caso la check box con le categorie, 
    `
    <div class="form-check fw-bolder">
        <input class="form-check-input" type="checkbox" value="${category}" id="checkbox${category}">
        <label class="form-check-label" for="checkbox${category}">
            ${category}
        </label>
    </div>

    `
    // lancio la call-back all'evento click sull'elemento selezionato
    categoryElement.addEventListener("input", allFilters)
    // seleziono il contenitore dove inserirle
    let categoriesWrapper = document.querySelector("#accordionCategoryWrapper")
    // e le aggancio
    categoriesWrapper.appendChild(categoryElement)

})
    // SLIDER


    // creo due arrey con valore 0 
    let minPrice = Number(annunci[0].price)
    let maxPrice = Number(annunci[0].price)

    // algoritmo per trovare il prezzo minimo e massimo
    annunci.forEach((annuncio, index) => {
        // salto il primo annuncio
        if (index == 0) return;

        // trasformo il prezzo del mio file json che è una stringa in un numero
        let price = Number(annuncio.price)

        // ciclo tutti i prezzi e mi trovo il minimo e il massimo valore per i prezzi così
        // da poterli utilizzare nello slider sotto 
        if (price > maxPrice) {
            maxPrice = price
        }
        if(price < minPrice){
            minPrice = price
        }
    })

    let slider = document.querySelector("#range-price");

    noUiSlider.create(slider, {
        start: [minPrice, maxPrice],
        connect: true,
        range: {
            'min': 0,
            'max': maxPrice
        }
    });

    let connect = slider.querySelector('.noUi-connect');
    connect.classList.add("presto-bg");
    
    slider.noUiSlider.on("change", allFilters)

    let search = document.querySelector("#searchName")
    search.addEventListener("input", allFilters)


    slider.noUiSlider.on("update", function() {
        let rangeMin = document.querySelector("#rangeMin")
        let rangeMax = document.querySelector("#rangeMax")
        let sliderParams = slider.noUiSlider.get();
        rangeMin.innerHTML = `${sliderParams [0]}`
        rangeMax.innerHTML = `${sliderParams [1]}`
    })



    annunciPopulate(annunci);
    // funzione di ricerca per tutto
    function allFilters(){
        let categoryInputs = document.querySelectorAll(".presto-category-find .form-check .form-check-input")
        let checkedCategories = []
        // ciclo tutte le categorie
        categoryInputs.forEach(function (inputElement) {
            // se trovo la corrispondeza con l'input aggiungo questa all'array creata
            if(inputElement.checked)
                checkedCategories.push(inputElement.value)
        })
        let search = document.querySelector("#searchName")
        let lowerSearch = search.value.toLowerCase();
        let sliderParams = slider.noUiSlider.get();
        let min = sliderParams [0];
        let max = sliderParams [1];
        let filteredAnnunci = annunci
                .filter((annuncio) => !lowerSearch || annuncio.name.toLowerCase().includes(lowerSearch))
                .filter((annuncio) => checkedCategories.length == 0 || checkedCategories.includes(annuncio.category))
                .filter((annuncio) => Number(annuncio.price) >= min && Number(annuncio.price) <= max)

        annunciPopulate(filteredAnnunci)
    }


    // funzione di ricerca tramite nome 
    function nameInputCallBack(){
        let search = document.querySelector("#searchName")
        let lowerSearch = search.value.toLowerCase()
        if (lowerSearch){
            // creo una variabile che ha come valore la funzione che filtra gli annunci mostrando solo la corrispondeza
                let filteredAnnunci= annunci.filter((annuncio) => annunci.name.toLowerCase().includes(lowerSearch))
                annunciPopulate(filteredAnnunci)
            } else {
                // se no mostra tutti gli annunci
                annunciPopulate(annunci)
            }
    }

    // funzione di ricerca tramite categoria
    function categoryInputCallback () {
        // creo una variabile selezionando il form che ho creato su
        let categoryInputs = document.querySelectorAll(".presto-category-find .form-check .form-check-input")
        // creo un'array
        let checkedCategories = []
        // ciclo tutte le categorie
        categoryInputs.forEach(function (inputElement) {
            // se trovo la corrispondeza con l'input aggiungo questa all'array creata
            if(inputElement.checked)
                checkedCategories.push(inputElement.value)
        })

        // se la lunghezza dell'array è maggiore di 0 è quindi è piena
        if (checkedCategories.length > 0){
            // creo una variabile che ha come valore la funzione che filtra gli annunci mostrando solo la corrispondeza
            let filteredAnnunci= annunci.filter((annuncio) => checkedCategories.includes(annuncio.category))
            annunciPopulate(filteredAnnunci)
        } else {
            // se no mostra tutti gli annunci
            annunciPopulate(annunci)
        }
    }

    // per mostrare il risultato della mia corrispondenza creo delle card e le aggancio al contenitore
    function annunciPopulate(annunci) {
        let annunciWrapper = document.querySelector("#annunci-wrapper")
        annunciWrapper.innerHTML= ""
        annunci.forEach(function (annuncio){
            let announcementElement = document.createElement("div")
            announcementElement.classList.add("presto-find-card")
            announcementElement.classList.add("mx-4")
            announcementElement.classList.add("mb-4")
            announcementElement.classList.add("text-center")

            
            announcementElement.innerHTML =
        // mi creo la mia card utilizzando come riferimento il mio file json

        `
        <h4 class="fw-bolder"> ${annuncio.category} </h4>
        <div class="d-flex flex-column">
            <img class="rounded rounded-3 margin-card" src="https://picsum.photos/200"
            <span> ${annuncio.name} </span>
            <span>€${annuncio.price}</span>
        </div>

        `
        let annnunciWrapper = document.querySelector("#annunci-wrapper")
        annnunciWrapper.appendChild(announcementElement)
        })
    }
})